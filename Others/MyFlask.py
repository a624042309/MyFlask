# -*- coding:utf-8 -*-
from flask import Flask
from flask import request
from flask import make_response
from flask import redirect
from flask import abort
from flask.ext.script import Manager

app = Flask(__name__)
manager = Manager(app)

@app.route('/')
def hello_world():
    # 上下文
    # user_agent = request.headers.get('User-Agent')
    # return u'<p>你的浏览器是：\n%s</p>' % user_agent

    # 状态码
    # return '<h1>Bad Reuest</h1>', 400

    # 响应对象  重定向响应
    # response = make_response('<h1>This document carries a cookie!</h1>')
    # response.set_cookie('answer', '42')
    # return response

    # redirect()辅助函数, 生成重定向响应
    return redirect('http://www.example.com')



@app.route('/user/<name>')
def user(name):
   return '<h1>Hello, %s</h1>' % name

@app.route('/user/<int:id>')
def get_user(id):
    user = load_user(id)
    if not user:
        abort(404)
    return '<h1>Hello, %s</h1>' % user

def load_user(id):
    if id == 1:
        user = 'Zhang San'
    elif id == 2:
        user = 'Li Si'
    else:
        user = None
    return user

if __name__ == '__main__':
    # app.run(debug=True)
    manager.run()
