#!/usr/bin/python
# -*- coding: utf-8 -*-

import os
import re
import json
import urllib2
import string
import traceback

from scrapy.spiders import Spider
from scrapy import Selector, Request

from goldmine.config.sentry_config import client
from goldmine.config.pipeline_config import cn_punishments_settings
from goldmine.utils.custom_exception import NetError
from goldmine.utils.httpbin_err import errback_httpbin
from goldmine.utils.format_string import cn_punishments_common_item


class Spider(Spider):
    name = '_spider'
    start_urls = [
        ''
    ]
    domain = ''
    custom_settings = cn_punishments_settings

    def parse(self, response):
        print 'response', response.url
        try:
            infoList = response.xpath('//*[@class="liebiaoys24"]/a')
            for info in infoList:
                href = info.xpath('@href').extract_first()
                if href:
                    if u'http' in href:
                        detail_page = href
                    else:
                        detail_page = ''.join(['', href])

                    print 'detail_page:', detail_page
                    yield Request(detail_page, callback=self.parse_detail, errback=errback_httpbin)

            pageList = response.xpath('//*')
            for page in pageList:
                title = page.xpath('string()').extract_first()
                if title and u'下一页' in title:
                    link = page.xpath('@href').extract_first()
                    if link:
                        next_page = ''.join(['', link])
                        print 'NextPage:', next_page
                        yield Request(next_page, callback=self.parse, errback=errback_httpbin)

        except NetError as e:
            client.captureException(message=e.message, fingerprint=e.fingerprint,
                                    tags={'custom_tag': e.tags.get('custom_tag'), 'spider': __file__.split(os.sep)[-1]})
        except Exception:
            traceback.print_exc()
            client.captureException(tags={'spider': __file__.split(os.sep)[-1]})

    def parse_detail(self, response):
        try:
            print '*' * 20
            print 'parse_detail:', response.url

            imgs = []
            if response.xpath('//*[@id="zoom"]'):
                # 文字内容 content
                content_tag = response.xpath('//*[@id="zoom"]')
                for text in content_tag:
                    content = text.xpath('string()').extract_first()
                    if content:
                        print content, '---'
                        item = cn_punishments_common_item(response, source=response.url, content=content,
                                                          country_code='CHN')
                        yield item

                # 图片内容 photo_img
                img_tag = response.xpath('//*[@id="zoom"]/img|//*[@id="zoom"]/p/img')
                for img in img_tag:
                    src = img.xpath('@src').extract_first()
                    if src:
                        src = urllib2.quote(src.encode('utf-8', 'replace'), safe=string.printable)
                        img_url = ''.join(['', src])
                        imgs.append(img_url)

                if len(imgs) > 0:
                    key = [str(x) for x in range(len(imgs))]
                    img_dict = dict(zip(key, imgs))
                    print 'photo_img:', img_dict
                    item = cn_punishments_common_item(response, source=response.url, photo_img=img_dict,
                                                      country_code='CHN')
                    yield item
            else:
                pass

        except NetError as e:
            client.captureException(message=e.message, fingerprint=e.fingerprint,
                                    tags={'custom_tag': e.tags.get('custom_tag'), 'spider': __file__.split(os.sep)[-1]})
        except Exception:
            traceback.print_exc()
            client.captureException(tags={'spider': __file__.split(os.sep)[-1]})


